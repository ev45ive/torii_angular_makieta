import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'

import 'rxjs/add/observable/of'

@Injectable()
export class ProductsService {

  constructor() { }

  search(query){
    return Observable.of( 
      this.products.filter(p => p.name.indexOf(query) != -1 )
    )
  }
  products = [
      {id:1, name:'Product Ala'},
      {id:2, name:'Product Alicja'},
      {id:3, name:'Product Bob'},
    ]

  getProducts(){
    return Observable.of(this.products)
  }

  cart = []

  getCartItems(){
    return this.cart;
  }

  addToCart(id){
    this.cart.push(id)
  }

}
