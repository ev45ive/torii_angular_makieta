import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Routing } from "./app.routing";
import { LoginComponent } from './containers/login.component';
import { RegisterComponent } from './containers/register.component';
import { HomeComponent } from './containers/home.component';
import { ProductComponent } from './containers/product.component';
import { CartComponent } from './containers/cart.component';
import { ProductsService } from "./services/products.service";
import { ProductsListComponent } from './views/products-list.component';
import { CartListComponent } from './views/cart-list.component';
import { AddToCartComponent } from './views/add-to-cart.component';
import { SearchComponent } from './containers/search.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProductComponent,
    CartComponent,
    ProductsListComponent,
    CartListComponent,
    AddToCartComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    Routing
  ],
  providers: [
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
