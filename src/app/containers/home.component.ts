import { Component, OnInit } from '@angular/core';
import { ProductsService } from "../services/products.service";

@Component({
  selector: 'app-home',
  template: `
    <div routerLink="/cart">Cart</div>
    <div routerLink="login">LogOut</div>
    <p>
      home works!
    </p>
    <div class="row">
      <div class="col">
        <products-list [products]="products | async">
        </products-list>
      </div>
      <div class="col">
        <cart-list [items]="items"></cart-list>
      </div>
    </div>
  `,
  styles: []
})
export class HomeComponent implements OnInit {

  constructor(private service:ProductsService) { }

  products
  items

  ngOnInit() {
    this.products = this.service.getProducts()
    this.items = this.service.getCartItems()
  }

}
