import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-product',
  template: `
    <div *ngIf="(params | async).id as id ">
      <div routerLink="/">Home</div>
      <p>
        product {{id}} works!
      </p>
      <div [routerLink]="['/cart',id]"> Add to cart </div>
    </div>
  `,
  styles: []
})
export class ProductComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }

  params

  ngOnInit() {
    this.params = this.route.params
  }

}
