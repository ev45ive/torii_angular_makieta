import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ProductsService } from "../services/products.service";

@Component({
  selector: 'app-cart',
  template: `
    <div routerLink="/">Home</div>
    <p>
      cart works!
    </p>
    <cart-list [items]="items"></cart-list>
  `,
  styles: []
})
export class CartComponent implements OnInit {

  constructor(private route:ActivatedRoute, 
              private router:Router,
              private service:ProductsService) { }

  items

  ngOnInit() {
    this.route.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.service.addToCart(id)
        this.router.navigate(['/cart'],{replaceUrl:true})
      }
      this.items = this.service.getCartItems()
    })
  }

}
