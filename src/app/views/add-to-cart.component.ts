import { Component, OnInit,Input } from '@angular/core';
import { ProductsService } from '../services/products.service'

@Component({
  selector: 'add-to-cart',
  template: `
    <button (click)="add()">Add To Cart</button>
  `,
  styles: []
})
export class AddToCartComponent implements OnInit {

  constructor(private service:ProductsService) { }

  @Input()
  product

  add(){
    this.service.addToCart(this.product)
  }

  ngOnInit() {
  }

}
