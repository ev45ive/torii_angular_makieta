import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'products-list',
  template: `  
    <div *ngFor="let product of products">
      <h3>{{ product.name }}</h3>

      <div [routerLink]="['/product',product.id]">
        Details
      </div>
      <add-to-cart [product]="product.name"></add-to-cart>
    </div>
  `,
  styles: []
})
export class ProductsListComponent implements OnInit {

  @Input()
  products

  constructor() { }

  ngOnInit() {
  }

}
