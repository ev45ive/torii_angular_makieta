import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cart-list',
  template: `
    <ul>
      <li *ngFor="let item of items">
        {{item}}
      </li>
    </ul>
  `,
  styles: []
})
export class CartListComponent implements OnInit {

  @Input()
  items
  
  constructor() { }

  ngOnInit() {
  }

}
