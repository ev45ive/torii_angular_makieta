import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from "./containers/login.component";
import { RegisterComponent } from "./containers/register.component";
import { HomeComponent } from "./containers/home.component";
import { ProductComponent } from "./containers/product.component";
import { CartComponent } from "./containers/cart.component";

const routes:Routes = [
    { path:'', component: HomeComponent },
    { path:'login', component: LoginComponent },
    { path:'register', component: RegisterComponent },
    { path:'product/:id', component: ProductComponent },
    { path:'cart', component: CartComponent },
    { path:'cart/:id', component: CartComponent },
]

export const Routing = RouterModule.forRoot(routes,{
    useHash:true
})